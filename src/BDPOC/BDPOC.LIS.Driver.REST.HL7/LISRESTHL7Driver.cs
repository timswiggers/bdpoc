﻿using System;
using System.Reflection;
using BDPOC.Connector;
using BDPOC.Protocol;
using BDPOC.Connector.REST;
using BDPOC.Protocols.HL7;

namespace BDPOC.LIS.Driver.REST.HL7
{
    public class LISRESTHL7Driver : ILISDriver
    {
        public LISRESTHL7Driver()
        {
            var type = GetType().GetTypeInfo();
            var assemblyName = type.Assembly.GetName();

            Name = assemblyName.Name;
            Version = assemblyName.Version;
        }

        public string Name { get; }
        public Version Version { get; }

        public IConnector GetConnector() => new RESTConnector();        
        public IProtocol GetProtocol() => new HL7Protocol();
    }
}
