﻿using System;
using BDPOC.Protocol;
using BDPOC.Protocol.Messages;

namespace BDPOC.Protocols.HL7
{
    public class HL7Protocol : IProtocol
    {
        public string Name => nameof(HL7Protocol);

        public object Decode(string message)
        {
            var parts = message.Split('|');
            var messageType = parts[0];

            if ("SayHelloMessage".Equals(messageType, StringComparison.OrdinalIgnoreCase))
            {
                var to = parts[1];
                return new SayHelloMessage {To = to};
            }

            return null;
        }

        public string Encode(object message)
        {
            switch (message)
            {
                case SayHelloMessage msg: return $"{nameof(SayHelloMessage)}|{msg.To}";
                case SayHelloMessageResponse msg: return $"{nameof(SayHelloMessageResponse)}|{msg.Value}";
            }
            return string.Empty;
        }
    }
}