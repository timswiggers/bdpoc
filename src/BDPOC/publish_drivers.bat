@echo off

md "C:\Temp\bdpoc\lisdrivers"

cd ".\BDPOC.LIS.Driver.REST.HL7"
rd /S /Q "C:\Temp\bdpoc\lisdrivers\bdpoc.lis.driver.rest.hl7\1.0.0.0\*"
dotnet.exe publish -c Debug -r win10-x64 -o "C:\Temp\bdpoc\lisdrivers\bdpoc.lis.driver.rest.hl7\1.0.0.0"
cd ..

cd ".\BDPOC.LIS.Driver.Files.HL7"
rd /S /Q "C:\Temp\bdpoc\lisdrivers\bdpoc.lis.driver.files.hl7\1.0.0.0\*"
dotnet.exe publish -c Debug -r win10-x64 -o "C:\Temp\bdpoc\lisdrivers\bdpoc.lis.driver.files.hl7\1.0.0.0"
cd ..