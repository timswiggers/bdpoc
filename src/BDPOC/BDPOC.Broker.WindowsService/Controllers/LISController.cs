﻿using BDPOC.Protocol.Messages;
using Microsoft.AspNetCore.Mvc;

namespace BDPOC.Broker.WindowsService.Controllers
{
    public class LISController : Controller
    {
        [HttpPost, Route("sayhello")]
        public SayHelloMessageResponse SayHello([FromBody] SayHelloMessage message)
        {
            return new SayHelloMessageResponse { Value = $"Hello there, {message.To}!" };
        }
    }
}