﻿using System;
using System.Threading;
using Microsoft.AspNetCore.Hosting;

namespace BDPOC.Broker.WindowsService
{
    class Program
    {
        private const string BrokerEndpoint = "http://localhost:5000";

        static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseWebListener()
                .UseUrls(BrokerEndpoint)
                .UseStartup<Startup>()
                .Build();

            host.Start();

            KeepBrokerRunning();
        }

        static void KeepBrokerRunning()
        {
            Console.WriteLine($"Broker is running at {BrokerEndpoint}");
            Console.WriteLine();

            var keepRunning = true;

            Console.CancelKeyPress += (sender, args) => keepRunning = false;

            while (keepRunning) Thread.Sleep(500);
        }
    }
}