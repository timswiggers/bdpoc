﻿using Microsoft.AspNetCore.Mvc;

namespace BDPOC.Connector.REST.Controllers
{
    public class HelloController : Controller
    {
        [Route("sayhello/to/{to}")]
        public string SayHello(string to) => $"Hello, {to}!";
    }
}
