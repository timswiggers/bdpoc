﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BDPOC.Driver.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;

namespace BDPOC.Connector.REST
{
    public class RESTConnector : IConnector, IConnectorMessageSender
    {
        public string Name => typeof(RESTConnector).FullName;
        public IConnectorMessageSender MessageSender => this;

        private IConnectorMessageAccepter _messageAccepter;
        private RESTConnectorSettings _settings;
        private IWebHost _webHost;

        public void Initialize(IConnectorMessageAccepter messageAccepter, ISettingsBag settingsBag)
        {
            _messageAccepter = messageAccepter;
            _settings = new RESTConnectorSettings(settingsBag);
            _webHost = CreateWebHost();
        }

        public async Task<string> SendAsync(string s)
        {
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = _settings.SenderHttpMethod,
                RequestUri = _settings.SenderUrl,
                Content = new StringContent(s)
            };

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(httpRequestMessage);
                return await response.Content.ReadAsStringAsync();
            }
        }

        public void Start()
        {
            if (_webHost == null)
            {
                throw new InvalidOperationException("The connector was not initialized yet");
            }

            _webHost.Start();
        }

        public void Stop()
        {
            _webHost?.Dispose();
        }

        private IWebHost CreateWebHost()
        {
            var baseUrl = new Uri($"http://localhost:{_settings.ReceiverPort}");
            var endpointUrl = new Uri(baseUrl, _settings.ReceiverPath);

            return new WebHostBuilder()
                .UseWebListener()
                .UseUrls(endpointUrl.AbsoluteUri)
                .Configure(app => app.Run(async context =>
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await context.Request.Body.CopyToAsync(memoryStream);
                        var content = Encoding.UTF8.GetString(memoryStream.ToArray());

                        var response = await _messageAccepter.AcceptAsync(content);

                        context.Response.ContentType = _settings.ReceiverResponseContentType;
                        await context.Response.WriteAsync(response);
                    }
                }))
                .Build();
        }
    }
}