﻿using System;
using System.Net.Http;
using BDPOC.Driver.Settings;

namespace BDPOC.Connector.REST
{
    public class RESTConnectorSettings
    {
        private readonly ISettingsBag _settingsBag;

        public RESTConnectorSettings(ISettingsBag settingsBag)
        {
            _settingsBag = settingsBag;
        }

        // Common settings
        public string ReceiverResponseContentType => _settingsBag.GetString(nameof(ReceiverResponseContentType));

        // Receiver settings
        public int ReceiverPort => _settingsBag.GetInt(nameof(ReceiverPort));
        public string ReceiverPath => _settingsBag.GetString(nameof(ReceiverPath));

        // Sender settings
        public Uri SenderUrl => new Uri(_settingsBag.GetString(nameof(SenderUrl)));
        public HttpMethod SenderHttpMethod => new HttpMethod(_settingsBag.GetString(nameof(SenderHttpMethod)));
    }
}