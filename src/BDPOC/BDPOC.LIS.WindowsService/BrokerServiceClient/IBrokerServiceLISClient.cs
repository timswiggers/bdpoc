﻿using System.Threading.Tasks;
using BDPOC.Protocol.Messages;

namespace BDPOC.LIS.WindowsService.BrokerServiceClient
{
    public interface IBrokerServiceLISClient
    {
        Task<SayHelloMessageResponse> SayHello(SayHelloMessage sayHello);
    }
}