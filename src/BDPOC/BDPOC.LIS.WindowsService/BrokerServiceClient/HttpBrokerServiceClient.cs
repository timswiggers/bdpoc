﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BDPOC.Connector;
using BDPOC.Protocol;
using BDPOC.Protocol.Messages;
using Newtonsoft.Json;

namespace BDPOC.LIS.WindowsService.BrokerServiceClient
{
    public class HttpBrokerServiceClient : IBrokerServiceLISClient, IConnectorMessageAccepter
    {
        private readonly IProtocol _protocol;

        public HttpBrokerServiceClient(IProtocol protocol)
        {
            _protocol = protocol;
        }

        public async Task<string> AcceptAsync(string message)
        {
            var messageDTO = _protocol.Decode(message);
            switch (messageDTO)
            {
                case SayHelloMessage sayHello:
                {
                    var response = await SayHello(sayHello);
                    return _protocol.Encode(response);
                }
            }

            return await Task.FromResult(string.Empty);
        }

        public async Task<SayHelloMessageResponse> SayHello(SayHelloMessage message)
        {
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://localhost:5000/sayhello"),
                Content = new StringContent(JsonConvert.SerializeObject(message), Encoding.UTF8, "application/json")
            };

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(httpRequestMessage);
                return JsonConvert.DeserializeObject<SayHelloMessageResponse>(await response.Content.ReadAsStringAsync());
            }
        }
    }
}