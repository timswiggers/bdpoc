﻿// https://weblogs.asp.net/ricardoperes/using-mef-in-net-core

using System;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;

namespace BDPOC.LIS.WindowsService.Drivers.Composition
{
    public static class ContainerConfigurationExtensions
    {
        public static ContainerConfiguration WithAssembliesIn(this ContainerConfiguration configuration, DirectoryInfo directory, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return WithAssembliesIn(configuration, directory, null, searchOption);
        }

        public static ContainerConfiguration WithAssembliesIn(this ContainerConfiguration configuration, DirectoryInfo directory, AttributedModelProvider conventions, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            var assemblies = Directory
                .GetFiles(directory.FullName, "*.Driver.*.dll", searchOption)
                .Select(LoadAssembly)
                .Where(a => a.Success)
                .Select(a => a.Assembly)
                .ToList();

            configuration = configuration.WithAssemblies(assemblies, conventions);

            return configuration;
        }

        private static (Assembly Assembly, bool Success) LoadAssembly(string assemblyPath)
        {
            try
            {
                var assembly = Assembly.LoadFile(assemblyPath);
                return (assembly, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return (null, false);
            }
        }
    }
}