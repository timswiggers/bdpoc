﻿using System.Collections.Generic;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.IO;
using BDPOC.LIS.Driver;

namespace BDPOC.LIS.WindowsService.Drivers.Composition
{
    public interface IDriverContainer<out TDriver>
    {
        IEnumerable<TDriver> Drivers { get; }
    }

    public class DriverContainer : IDriverContainer<ILISDriver>
    {
        private readonly CompositionHost _compositionHost;

        private DriverContainer(DirectoryInfo driverDirectory, CompositionHost compositionHost)
        {
            DriverDirectory = driverDirectory;
            _compositionHost = compositionHost;
        }

        public DirectoryInfo DriverDirectory { get; }

        public IEnumerable<ILISDriver> Drivers => _compositionHost.GetExports<ILISDriver>();

        public static DriverContainer Create(string driversRootPath)
        {
            var driversRootDirectory = ValidateDirectoryPath(driversRootPath);

            var driverContainerConventions = CreateConventionsFor<ILISDriver>();
            var driverContainerConfiguration = new ContainerConfiguration()
                .WithAssembliesIn(driversRootDirectory, driverContainerConventions, SearchOption.AllDirectories);

            var compositionHost = driverContainerConfiguration.CreateContainer();

            return new DriverContainer(driversRootDirectory, compositionHost);
        }

        private static DirectoryInfo ValidateDirectoryPath(string path)
        {
            var directory = new DirectoryInfo(path);

            if (!directory.Exists)
            {
                throw new DirectoryNotFoundException($"Driver directory not found: {path}");
            }

            return directory;
        }

        private static AttributedModelProvider CreateConventionsFor<TDriver>()
        {
            var conventions = new ConventionBuilder();

            conventions
                .ForTypesDerivedFrom<TDriver>()
                .Export<TDriver>()
                .Shared();

            return conventions;
        }
    }
}