﻿using System;
using System.Collections.Generic;
using System.Linq;
using BDPOC.Driver.Settings;

namespace BDPOC.LIS.WindowsService.Drivers.Hosting
{
    public static class DriverHostExtensions
    {
        public static DriverHostBuilder UseDriverDirectory(this DriverHostBuilder builder, string directory)
        {
            builder.DriverDirectory = directory;
            return builder;
        }

        public static DriverHostBuilder UseDriver(this DriverHostBuilder builder, string driverName)
        {
            builder.DriverName = driverName;
            return builder;
        }

        public static DriverHostBuilder UseDriverSettings(this DriverHostBuilder builder, IDictionary<string, object> settings)
        {
            builder.DriverSettings = new GenericConnectorSettings(settings);
            return builder;
        }

        private class GenericConnectorSettings : ISettingsBag
        {
            private readonly IDictionary<string, object> _settings;

            public GenericConnectorSettings(IDictionary<string, object> settings)
            {
                _settings = settings;
            }

            public string[] GetSettingKeys() => _settings.Keys.ToArray();

            public int GetInt(string key) => Convert.ToInt32(GetString(key));
            public string GetString(string key) => _settings.ContainsKey(key) ? Convert.ToString(_settings[key]) : null;
        }
    }
}
