﻿using BDPOC.Driver.Settings;

namespace BDPOC.LIS.WindowsService.Drivers.Hosting
{
    public interface IDriverHost
    {
        string DriverName { get; }
        ISettingsBag DriverSettings { get; }

        void Start();
        void Stop();
    }
}