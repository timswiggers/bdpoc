﻿using BDPOC.Connector;
using BDPOC.Driver.Settings;
using BDPOC.LIS.Driver;
using BDPOC.LIS.WindowsService.BrokerServiceClient;
using BDPOC.Protocol;

namespace BDPOC.LIS.WindowsService.Drivers.Hosting
{
    public class LISDriverHost : IDriverHost
    {
        private readonly ILISDriver _driver;
        private readonly ISettingsBag _driverSettings;

        private IConnector _connector;
        private IProtocol _protocol;

        public LISDriverHost(ILISDriver driver, ISettingsBag driverSettings)
        {
            _driver = driver;
            _driverSettings = driverSettings;
        }

        public string DriverName => _driver?.Name;
        public ISettingsBag DriverSettings => _driverSettings;

        public void Start()
        {
            _connector = _driver.GetConnector();
            _protocol = _driver.GetProtocol();

            Connect(_connector, to: BrokerServiceLISClient(usingProtocol: _protocol));

            _connector.Start();
        }

        private void Connect(IConnector connector, IConnectorMessageAccepter to)
        {
            connector.Initialize(to, _driverSettings);
        }

        private IConnectorMessageAccepter BrokerServiceLISClient(IProtocol usingProtocol)
        {
            return new HttpBrokerServiceClient(usingProtocol);
        }

        public void Stop() => _connector?.Stop();
    }
}