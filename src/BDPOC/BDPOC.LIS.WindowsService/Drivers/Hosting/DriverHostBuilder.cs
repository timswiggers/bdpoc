﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using BDPOC.Driver.Settings;
using BDPOC.LIS.WindowsService.Drivers.Composition;
using BDPOC.LIS.Driver;

namespace BDPOC.LIS.WindowsService.Drivers.Hosting
{
    public class DriverHostBuilder
    {
        public IDriverHost Build()
        {
            var driverContainer = DriverContainer.Create(DriverDirectory);
            var drivers = driverContainer.Drivers.ToList();
            if (drivers.Count == 0)
            {
                throw new Exception($"No drivers found in '{driverContainer.DriverDirectory.FullName}'");
            }

            var driver = drivers.SingleOrDefault(d => d.Name?.Equals(DriverName) ?? false);
            if (driver == null)
            {
                throw new Exception($"Unknown driver '{DriverName}'");
            }

            EnableDriverDependencyAssemblyResolve(driver);

            return new LISDriverHost(driver, DriverSettings);
        }

        private void EnableDriverDependencyAssemblyResolve(ILISDriver driver)
        {
            var name = driver.Name;
            var version = driver.Version.ToString();
            var directory = Path.Combine(DriverDirectory, name, version);

            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                // Ignore missing resources
                if (args.Name.Contains(".resources"))
                {
                    return null;
                }

                // check for assemblies already loaded
                var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
                if (assembly != null)
                {
                    return assembly;
                }

                // Try to load by filename - split out the filename of the full assembly name
                // and append the base path of the original assembly (ie. look in the same dir)
                var filename = args.Name.Split(',')[0] + ".dll".ToLower();
                var asmFile = Path.Combine(directory, filename);

                return Assembly.LoadFrom(asmFile);
            };
        }

        public string DriverName { get; set; }
        public string DriverDirectory { get; set; }
        public ISettingsBag DriverSettings { get; set; }
    }
}
