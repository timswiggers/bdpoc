﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BDPOC.Driver.Settings;
using BDPOC.LIS.WindowsService.Drivers.Hosting;

namespace BDPOC.LIS.WindowsService
{
    class Program
    {
        static void Main()
        {
            var driverHost = new DriverHostBuilder()
                .UseDriverDirectory(@"C:\temp\bdpoc\lisdrivers")

                //.UseDriver("BDPOC.LIS.Driver.Files.HL7")
                //.UseDriverSettings(new Dictionary<string, object>
                //{
                //    { "Filter", "*.txt" },
                //    { "InDirectory", @"C:\Temp\bdpoc\lisfiles\hl7in" },
                //    { "OutDirectory", @"C:\Temp\bdpoc\lisfiles\hl7out" },
                //    { "ProcessedDirectory", @"C:\Temp\bdpoc\lisfiles\hl7in_processed" }
                //})

                .UseDriver("BDPOC.LIS.Driver.REST.HL7")
                .UseDriverSettings(new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                {
                    { "ReceiverPort", 5001 },
                    { "ReceiverPath", "lis" },
                    { "ReceiverResponseContentType", "text/plain" },
                    { "SenderUrl", null },
                    { "SenderHttpMethod", "POST" }
                })

                .Build();
            driverHost.Start();

            KeepDriverRunning(driverHost);
        }

        static void KeepDriverRunning(IDriverHost driverHost)
        {
            Console.WriteLine($"LIS Service is running driver '{driverHost.DriverName}'");
            Console.WriteLine();

            ListSettings(driverHost.DriverSettings);

            var keepRunning = true;

            Console.CancelKeyPress += (sender, args) => keepRunning = false;

            while (keepRunning) Thread.Sleep(500);
        }

        private static void ListSettings(ISettingsBag s)
        {
            var keys = s.GetSettingKeys();
            var keyLongestLength = keys.Select(k => k.Length).Max();

            foreach (var key in keys)
            {
                Console.WriteLine($"- {key.PadRight(keyLongestLength)} {s.GetString(key)}");
            }

            Console.WriteLine();
        }
    }
}