﻿namespace BDPOC.Protocol.Messages
{
    public class SayHelloMessage
    {
        public string To { get; set; }
    }
}