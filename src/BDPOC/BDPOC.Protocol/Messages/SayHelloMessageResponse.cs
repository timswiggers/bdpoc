﻿namespace BDPOC.Protocol.Messages
{
    public class SayHelloMessageResponse
    {
        public string Value { get; set; }
    }
}