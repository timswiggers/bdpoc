﻿namespace BDPOC.Protocol
{
    public interface IProtocol
    {
        string Name { get; }

        object Decode(string message);
        string Encode(object message);
    }
}