﻿using System;
using System.Reflection;
using BDPOC.Connector;
using BDPOC.Protocol;
using BDPOC.Connector.Files;

namespace BDPOC.LIS.Driver.Files.HL7
{
    public class LISFilesHL7Driver : ILISDriver
    {
        public LISFilesHL7Driver()
        {
            var type = GetType().GetTypeInfo();
            var assemblyName = type.Assembly.GetName();

            Name = assemblyName.Name;
            Version = assemblyName.Version;
        }

        public string Name { get; }
        public Version Version { get; }

        public IConnector GetConnector() => new FilesConnector();
        public IProtocol GetProtocol() => throw new NotImplementedException();
    }
}
