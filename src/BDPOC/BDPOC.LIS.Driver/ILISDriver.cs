﻿using System;
using BDPOC.Connector;
using BDPOC.Protocol;

namespace BDPOC.LIS.Driver
{
    public interface ILISDriver
    {
        string Name { get; }
        Version Version { get; }

        IConnector GetConnector();
        IProtocol GetProtocol();
    }
}