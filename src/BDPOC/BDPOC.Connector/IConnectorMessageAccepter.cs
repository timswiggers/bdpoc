﻿using System.Threading.Tasks;

namespace BDPOC.Connector
{
    public interface IConnectorMessageAccepter
    {
        /// <summary>
        /// Accept a message into the broker
        /// </summary>
        /// <param name="message">The message from the external system</param>
        /// <returns>The response from the broker</returns>
        Task<string> AcceptAsync(string message);
    }
}