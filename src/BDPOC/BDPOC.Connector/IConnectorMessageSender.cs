﻿using System.Threading.Tasks;

namespace BDPOC.Connector
{
    public interface IConnectorMessageSender
    {
        /// <summary>
        /// Send a message to the external system
        /// </summary>
        /// <param name="message">The message from the broker</param>
        /// <returns>THe message from the external system</returns>
        Task<string> SendAsync(string message);
    }
}