﻿using BDPOC.Driver.Settings;

namespace BDPOC.Connector
{
    public interface IConnector
    {
        string Name { get; }

        IConnectorMessageSender MessageSender { get; }

        void Initialize(IConnectorMessageAccepter messageAccepter, ISettingsBag settings);

        void Start();
        void Stop();
    }
}
