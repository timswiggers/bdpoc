﻿namespace BDPOC.Instrument.Driver.Canto
{
    public class CantoInstrumentDriver : IInstrumentDriver
    {
        public string Name => nameof(CantoInstrumentDriver);
    }
}