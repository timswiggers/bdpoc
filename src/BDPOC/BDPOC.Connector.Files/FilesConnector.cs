﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using BDPOC.Driver.Settings;

namespace BDPOC.Connector.Files
{
    public class FilesConnector : IConnector, IConnectorMessageSender
    {
        public string Name => typeof(FilesConnector).FullName;
        public IConnectorMessageSender MessageSender => this;

        private IConnectorMessageAccepter _messageAccepter;
        private FilesConnectorSettings _settings;
        private FileSystemWatcher _fileSystemWatcher;

        public void Initialize(IConnectorMessageAccepter messageAccepter, ISettingsBag settingsBag)
        {
            _messageAccepter = messageAccepter;
            _settings = new FilesConnectorSettings(settingsBag);

            Directory.CreateDirectory(_settings.InDirectory.FullName);
            Directory.CreateDirectory(_settings.OutDirectory.FullName);
            Directory.CreateDirectory(_settings.ProcessedDirectory.FullName);

            _fileSystemWatcher = CreateFileWatcher();
        }

        public Task<string> SendAsync(string s)
        {
            throw new NotImplementedException();
        }

        public void Start() => _fileSystemWatcher.EnableRaisingEvents = true;
        public void Stop() => _fileSystemWatcher.EnableRaisingEvents = false;

        private FileSystemWatcher CreateFileWatcher()
        {
            var fileWatcher = new FileSystemWatcher
            {
                Path = _settings.InDirectory.FullName,
                Filter = _settings.Filter ?? "*.*"
            };

            fileWatcher.Created += async (sender, args) =>
            {
                var file = new FileInfo(args.FullPath);
                if (!file.Exists)
                {
                    return;
                }

                using (var inFileStream = file.OpenRead())
                using (var inMemoryStream = new MemoryStream())
                {
                    await inFileStream.CopyToAsync(inMemoryStream);
                    var content = Encoding.UTF8.GetString(inMemoryStream.ToArray());

                    var response = await _messageAccepter.AcceptAsync(content);
                    var responseFile = Path.Combine(_settings.OutDirectory.FullName, file.Name);
                    var responseContent = Encoding.UTF8.GetBytes(response);

                    using (var outMemoryStream = new MemoryStream(responseContent))
                    using (var outFileStream = new FileInfo(responseFile).OpenWrite())
                    {
                        await outMemoryStream.CopyToAsync(outFileStream);
                    }
                }

                var processedFile = Path.Combine(_settings.ProcessedDirectory.FullName, file.Name);

                file.CopyTo(processedFile, overwrite: true);
                file.Delete();
            };

            return fileWatcher;
        }
    }
}
