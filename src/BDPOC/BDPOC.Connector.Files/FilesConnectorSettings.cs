﻿using System.IO;
using BDPOC.Driver.Settings;

namespace BDPOC.Connector.Files
{
    public class FilesConnectorSettings
    {
        private readonly ISettingsBag _settingsBag;

        public FilesConnectorSettings(ISettingsBag settingsBag)
        {
            _settingsBag = settingsBag;
        }

        public string Filter => _settingsBag.GetString(nameof(Filter));
        public DirectoryInfo InDirectory => new DirectoryInfo(_settingsBag.GetString(nameof(InDirectory)));
        public DirectoryInfo OutDirectory => new DirectoryInfo(_settingsBag.GetString(nameof(OutDirectory)));
        public DirectoryInfo ProcessedDirectory => new DirectoryInfo(_settingsBag.GetString(nameof(ProcessedDirectory)));
    }
}
