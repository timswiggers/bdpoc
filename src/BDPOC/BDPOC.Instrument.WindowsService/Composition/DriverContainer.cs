﻿using System.Collections;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.IO;
using BDPOC.Instrument.Driver;

namespace BDPOC.Instrument.WindowsService.Composition
{
    public interface IDriverContainer
    {
        IEnumerable<IInstrumentDriver> Drivers { get; }
    }

    public class DriverContainer : IDriverContainer
    {
        private readonly CompositionHost _compositionHost;

        private DriverContainer(CompositionHost compositionHost)
        {
            _compositionHost = compositionHost;
        }

        public IEnumerable<IInstrumentDriver> Drivers => _compositionHost.GetExports<IInstrumentDriver>();

        public static DriverContainer CreateForDriversLocatedIn(string directoryPath)
        {
            var driversDirectory = ValidateDirectoryPath(directoryPath);
            var driverContainerConventions = CreateConventionsFor<IInstrumentDriver>();
            var driverContainerConfiguration = new ContainerConfiguration()
                .WithAssembliesIn(driversDirectory, driverContainerConventions, SearchOption.AllDirectories);

            var compositionHost = driverContainerConfiguration.CreateContainer();

            return new DriverContainer(compositionHost);
        }

        private static DirectoryInfo ValidateDirectoryPath(string path)
        {
            var directory = new DirectoryInfo(path);

            if (!directory.Exists)
            {
                throw new DirectoryNotFoundException($"Driver directory not found: {path}");
            }

            return directory;
        }

        private static AttributedModelProvider CreateConventionsFor<TDriver>()
        {
            var conventions = new ConventionBuilder();

            conventions
                .ForTypesDerivedFrom<TDriver>()
                .Export<TDriver>()
                .Shared();

            return conventions;
        }
    }
}