﻿using System;
using BDPOC.Instrument.WindowsService.Composition;

namespace BDPOC.Instrument.WindowsService
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Loading drivers...");

            var driverContainer = DriverContainer.CreateForDriversLocatedIn(@".\Drivers");
            var drivers = driverContainer.Drivers;

            foreach (var driver in drivers)
            {
                Console.WriteLine($"- {driver.Name}");
            }

            Console.ReadLine();
        }
    }
}