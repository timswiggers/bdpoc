﻿namespace BDPOC.Instrument.Driver
{
    public interface IInstrumentDriver
    {
        string Name { get; }
    }
}