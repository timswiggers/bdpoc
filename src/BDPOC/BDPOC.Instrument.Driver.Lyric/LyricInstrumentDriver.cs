﻿namespace BDPOC.Instrument.Driver.Canto
{
    public class LyricInstrumentDriver : IInstrumentDriver
    {
        public string Name => nameof(LyricInstrumentDriver);
    }
}
