namespace BDPOC.Driver.Settings
{
    public interface ISettingsBag
    {
        string GetString(string key);
        int GetInt(string key);

        string[] GetSettingKeys();
    }
}